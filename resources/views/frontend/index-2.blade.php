@extends('frontend.layouts.app'.config('theme_layout'))
@php $no_footer = true; @endphp

@section('title', trans('labels.frontend.home.title').' | '.app_name())
@section('meta_description', '')
@section('meta_keywords','')

@push("after-styles")
    <style>
        #search-course {
            padding-bottom: 125px;
        }
        .my-alert{
            position: absolute;
            z-index: 10;
            left: 0;
            right: 0;
            top: 25%;
            width: 50%;
            margin: auto;
            display: inline-block;
        }

      
		.section-title .subtitle {
    color:black;
    letter-spacing: 0px !important;
    position: relative;
}
    </style>
@endpush
@php
    $footer_data = json_decode(config('footer_data'));
@endphp
@section('content')
    @if(session()->has('alert'))
        <div class="alert alert-light alert-dismissible fade my-alert show">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>{{session('alert')}}</strong>
        </div>
    @endif

    <!-- Start of slider section
     ============================================= -->
    @include('frontend.layouts.partials.slider')

    <!-- End of slider section
            ============================================= -->


    @if($sections->sponsors->status == 1)
        @if(count($sponsors) > 0 )
    <!-- Start of sponsor section
        ============================================= -->
    <div id="sponsor" class="sponsor-section sponsor-2">
        <div class="container">
            <div class="sponsor-item">
                @foreach($sponsors as $sponsor)
                    <div class="sponsor-pic text-center">
                        <a href="{{ ($sponsor->link != "") ? $sponsor->link : '#' }}">
                            <img src={{asset("storage/uploads/".$sponsor->logo)}} alt="{{$sponsor->name}}">
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    @endif
   @endif
    <!-- End of sponsor section
        ============================================= -->


    <!-- Start popular course
        ============================================= -->
    @if($sections->popular_courses->status == 1)
        @include('frontend.layouts.partials.popular_courses')
    @endif
    <!-- End popular course
    ============================================= -->

    @if($sections->search_section->status == 1)
        <!-- Start of Search Courses
    ============================================= -->
        <section id="search-course" class="search-course-section home-secound-course-search backgroud-style">   
            <div class="container">
                <div class="section-title mb20 headline text-center">
                    <span class="subtitle text-uppercase" style="color:white !important;letter-spacing: 0px !important;">@lang('labels.frontend.home.learn_new_skills')</span>
                    <h3>@lang('labels.frontend.home.search_courses')</h3>
                </div>
                <div class="search-course mb30 relative-position">
                    <form action="{{route('search')}}" method="get">
                        <div class="input-group search-group">
                            <input class="course" name="q" type="text"
                                   placeholder="@lang('labels.frontend.home.search_course_placeholder')">

                            <select name="category" class="select form-control">
                                @if(count($categories) > 0 )
                                    <option value="">@lang('labels.frontend.course.select_category')</option>
                                    @foreach($categories as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>

                                    @endforeach
                                @else
                                    <option>>@lang('labels.frontend.home.no_data_available')</option>
                                @endif

                            </select>
                            <div class="nws-button position-relative text-center  gradient-bg text-capitalize">
                                <button type="submit" value="Submit">@lang('labels.frontend.home.search_course')</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="search-counter-up">
                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <div class="counter-icon-number ">
                                <div class="counter-icon">
                                    <i class="text-gradiant flaticon-graduation-hat"></i>
                                </div>
                                <div class="counter-number">
                                    <span class=" bold-font">{{$total_students}}</span>
                                    <p>@lang('labels.frontend.home.students_enrolled')</p>
                                </div>
                            </div>
                        </div>
                        <!-- /counter -->

                        <div class="col-md-4 col-sm-4">
                            <div class="counter-icon-number ">
                                <div class="counter-icon">
                                    <i class="text-gradiant flaticon-book"></i>
                                </div>
                                <div class="counter-number">
                                    <span class=" bold-font">{{$total_courses}}</span>
                                    <p>@lang('labels.frontend.home.online_available_courses')</p>
                                </div>
                            </div>
                        </div>
                        <!-- /counter -->


                        <div class="col-md-4 col-sm-4">
                            <div class="counter-icon-number ">
                                <div class="counter-icon">
                                    <i class="text-gradiant flaticon-group"></i>
                                </div>
                                <div class="counter-number">
                                    <span class=" bold-font">{{$total_teachers}}</span>
                                    <p>@lang('labels.frontend.home.teachers')</p>
                                </div>
                            </div>
                        </div>
                        <!-- /counter -->
                    </div>
                </div>
            </div>
        </section>
        <!-- End of Search Courses
            ============================================= -->
    @endif

    @if($sections->latest_news->status == 1)
        <!-- Start latest section
        ============================================= -->
        @include('frontend.layouts.partials.latest_news')
        <!-- End latest section
            ============================================= -->
    @endif


    @if($sections->featured_courses->status == 1)
        <!-- Start of best course
        ============================================= -->
        @include('frontend.layouts.partials.browse_courses')
        <!-- End of best course
            ============================================= -->
    @endif



    @if($sections->faq->status == 1)
        <!-- Start FAQ section
        ============================================= -->
        @include('frontend.layouts.partials.faq',['classes' => 'faq-secound-home-version backgroud-style'])
        <!-- End FAQ section
            ============================================= -->
    @endif


    @if($sections->course_by_category->status == 1)
        <!-- Start Course category
        ============================================= -->
        <section id="course-category" class="course-category-section home-secound-version">
            <div class="container">
                <div class="section-title mb20 headline text-left">
                    <span class="subtitle ml42 text-uppercase" >@lang('labels.frontend.layouts.partials.courses_categories')</span>
                    <h3>@lang('labels.frontend.layouts.partials.browse_course_by_category')</h3>
                </div>
                <div class="category-item category-slide-item">
                    @if($course_categories)
                        @foreach($course_categories as $key=>$category)
                            @if($key%2 == 0)
                                <div class="category-slide-content">
                                    @endif
                                    <a href="{{route('courses.category',['category'=>$category->slug])}}">
                                        <div class="category-icon-title text-center">
                                            <div class="category-icon">
                                                <i class="text-gradiant {{$category->icon}}"></i>
                                            </div>
                                            <div class="category-title">
                                                <h4>{{$category->name}}</h4>
                                            </div>
                                        </div>
                                    </a>
                                    @if($key%2 == 1)
                                </div>
                            @endif
                        @endforeach
                    @endif
                </div>
            </div>
        </section>
        <!-- End Course category
            ============================================= -->
    @endif

    @if($sections->testimonial->status == 1)
        <!-- Start secound testimonial section
        ============================================= -->
    <section id="testimonial-secound" class="secound-testimoinial-section">
        <div class="container">
            <div class="testimonial-slide">
                <div class="section-title mb35 headline text-center">
                    <span class="subtitle text-uppercase font-weight-bolder">@lang('labels.frontend.home.what_they_say_about_us')</span>
                    <h3>@lang('labels.frontend.layouts.partials.students_testimonial')</h3>
                </div>
                @if($testimonials->count() > 0)
                <div class="testimonial-secound-slide-area">
                    @foreach($testimonials as $item)

                    <div class="student-qoute text-center">
                        <p>{{$item->content}}</p>
                        <div class="student-name-designation">
                            <span class="st-name bold-font">{{$item->name}}  </span>
                            <span class="st-designation">{{$item->occupation}}</span>
                        </div>
                    </div>
                    @endforeach
                </div>
                @endif
            </div>
        </div>
    </section>
    <!-- End secound testimonial section
        ============================================= -->
    @endif


    @if($sections->teachers->status == 1)
        <!-- Start secound teacher section
        ============================================= -->
    <section id="teacher-2" class="secound-teacher-section">
        <div class="container">
            <div class="section-title mb35 headline text-left">
                <span class="subtitle ml42  text-uppercase">@lang('labels.frontend.home.our_professionals')</span>
                <h3>{{env('APP_NAME')}} <span>@lang('labels.frontend.home.teachers').</span></h3>
            </div>
            <div class="teacher-secound-slide">
                @if(count($teachers)> 0)
                    @foreach($teachers as $item)
                        <div class="teacher-img-text relative-position text-center">
                            <div class="teacher-img-social relative-position" >
                                <img height="200px" width="200px" src="{{$item->picture}}" alt="{{$item->full_name}}">
                                <div class="blakish-overlay"></div>
                                <div class="teacher-social-list ul-li">
                                    <ul>
                                        <li><a href="{{'mailto:'.$item->email}}"><i class="fa fa-envelope"></i></a></li>
                                        <li><a href="{{route('admin.messages',['teacher_id'=>$item->id])}}"><i class="fa fa-comments"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="teacher-name-designation mt15">
                                <span class="teacher-name">{{$item->full_name}}</span>
                                {{--<span class="teacher-designation">Mobile Apps</span>--}}
                            </div>
                        </div>
                    @endforeach
                @endif

            </div>

            <div class="genius-btn gradient-bg text-center text-uppercase ul-li-block bold-font">
                <a href="{{route('teachers.index')}}">@lang('labels.frontend.home.all_teachers') <i class="fas fa-caret-right"></i></a>
            </div>
        </div>
    </section>
    <!-- End teacher section
        ============================================= -->
    @endif

    @if($sections->contact_us->status == 1)
        <!-- Start Of  Footer Section
        ============================================= -->
  <section id="footer-section" class="footer-section">
        <div class="container text-bold text-center">
            <div class="row">
        
                    <div class=" col-md-3  ">
                        <div class="demo"><li class=" text-white m-3"><a class="" href="#">Teach LearnYas</a></li></div>
                        <div class="demo"><li class=" text-white m-3"><a class="" href="#">About US</a></li></div>
                        <div class="demo"><li class=" text-white m-3"><a class="" href="#">Contact US </a></li></div>
                        <div class="demo"><li class=" text-white m-3"><a class="" href="#">Blog</a></li></div>
                        <div class="demo"><li class=" text-white m-3"><a class="" href="#">Bundles </a></li></div>
                      
                    </div>
                
                
                <div class="col-md-3 ">
                    <div class="demo"> <li class="text-white m-3"><a class=" "href="#">Terms</a></li></div>
                    <div class="demo"> <li class="text-white m-3"><a class=" "href="#">Privacy policy</a></li></div>
                    <div class="demo"> <li class="text-white m-3"><a class=" "href="#">Courses</a></li></div>
                    <div class="demo"> <li class="text-white m-3"><a class=" "href="#">Forums</a></li></div>
                    <div class="demo"> <li class="text-white m-3"><a class=" "href="#">Bundles</a></li></div>
                  
                </div>
                <div class="brand-footer-section ">
                    <a class="navbar-brand " href="{{url('/')}}"><img
                        src="{{asset("storage/logos/".config('logo_b_image'))}}" alt="logo"></a>
                      
                       

                </div>
            </div>
          <div class="icon-footer ">
                <a class="text-white m-3 "><i class="fab fa-facebook-f"></i></a>
                <a class="text-white  m-3 "><i class="fab fa-youtube"></i></a>
                <a class="text-white  m-3 "><i class="fab fa-linkedin-in"></i></i></a>
          </div>
          <div class="demo"> <li class=" "><a class="text-white "href="#">© Copyright Massive ET. All rights reserved</a></li></div>

           
        </div>
    </section>
    <!-- ENd Of Footer Section
        ============================================= -->
    @endif

@endsection

@push('after-scripts')
    <script>
        $('ul.product-tab').find('li:first').addClass('active');
    </script>
@endpush